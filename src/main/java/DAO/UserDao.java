package DAO;

import ENTITY.User;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;

public class UserDao {

    public void insertUser(User user)
    {
        EntityManager entityManager= FactoryProvider.getEntityManagerFactory().createEntityManager();
        entityManager.getTransaction().begin();
        entityManager.persist(user);
        entityManager.getTransaction().commit();
        entityManager.close();
    }
}
