package DAO;

import ENTITY.Doctor;

import javax.persistence.EntityManager;

public class DoctorDao {

    public void insertDoctor(Doctor doctor)
    {
        EntityManager entityManager= FactoryProvider.getEntityManagerFactory().createEntityManager();
        entityManager.getTransaction().begin();
        entityManager.persist(doctor);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    public void updateAStatus(Doctor doctor)
    {
        EntityManager entityManager= FactoryProvider.getEntityManagerFactory().createEntityManager();
        entityManager.getTransaction().begin();
        doctor.setStatus("AVAILABLE");
        entityManager.merge(doctor);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    public void updateNStatus(Doctor doctor)
    {
        EntityManager entityManager= FactoryProvider.getEntityManagerFactory().createEntityManager();
        entityManager.getTransaction().begin();
        doctor.setStatus("NOT AVAILABLE");
        entityManager.merge(doctor);
        entityManager.getTransaction().commit();
        entityManager.close();
    }
}
