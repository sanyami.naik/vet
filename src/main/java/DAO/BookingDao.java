package DAO;

import ENTITY.Appointment;

import javax.persistence.EntityManager;

public class BookingDao {

    public void insertBooking(Appointment appointment)
    {
        EntityManager entityManager= FactoryProvider.getEntityManagerFactory().createEntityManager();
        entityManager.getTransaction().begin();
        entityManager.persist(appointment);
        entityManager.getTransaction().commit();
        entityManager.close();
    }
}
