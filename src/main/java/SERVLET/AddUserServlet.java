package SERVLET;

import DAO.FactoryProvider;
import DAO.UserDao;
import ENTITY.User;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

@WebServlet("/AddUserServlet")
public class AddUserServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        PrintWriter printWriter=resp.getWriter();
        resp.setContentType("text/html");

        String uName=req.getParameter("uName");
        Long uNumber=Long.parseLong(req.getParameter("uNumber").trim());

        EntityManager entityManager= FactoryProvider.getEntityManagerFactory().createEntityManager();
        CriteriaBuilder criteriaBuilder= entityManager.getCriteriaBuilder();
        CriteriaQuery criteriaQuery= criteriaBuilder.createQuery();
        Root<User> root= criteriaQuery.from(User.class);

        Query query= entityManager.createQuery(criteriaQuery.select(root));
        List<User> userList=query.getResultList();

        if(userList.stream().filter(e->e.getUserNumber().equals(uNumber)).count()==0)
        {
            User user=new User(uName,uNumber);
            UserDao userDao=new UserDao();
            userDao.insertUser(user);
            RequestDispatcher requestDispatcher= req.getRequestDispatcher("index.html");
            printWriter.print("User added succesfully");
            requestDispatcher.include(req,resp);
        }
        else {
            RequestDispatcher requestDispatcher= req.getRequestDispatcher("index.html");
            printWriter.print("User already exists");
            requestDispatcher.include(req,resp);
        }


    }
}
