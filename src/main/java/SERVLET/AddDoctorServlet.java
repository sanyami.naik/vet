package SERVLET;

import DAO.DoctorDao;
import DAO.FactoryProvider;
import DAO.UserDao;
import ENTITY.Doctor;
import ENTITY.User;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

@WebServlet("/AddDoctorServlet")
public class AddDoctorServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        PrintWriter printWriter = resp.getWriter();
        resp.setContentType("text/html");

        String dName = req.getParameter("dName");
        String dStatus = req.getParameter("dStatus").toUpperCase().trim();
        String doctorPassword = req.getParameter("dPassword");
        int doctorFees = Integer.parseInt(req.getParameter("dFees"));
        Long doctorNumber = Long.parseLong(req.getParameter("dNumber").trim());


        EntityManager entityManager = FactoryProvider.getEntityManagerFactory().createEntityManager();
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery criteriaQuery = criteriaBuilder.createQuery();
        Root<Doctor> root = criteriaQuery.from(Doctor.class);

        Query query = entityManager.createQuery(criteriaQuery.select(root));
        List<Doctor> doctorList = query.getResultList();

        if (doctorList.stream().filter(e -> e.getDoctorNumber().equals(doctorNumber)).count() == 0) {
            Doctor doctor = new Doctor(dName, dStatus, doctorPassword, doctorFees, doctorNumber);
            DoctorDao doctorDao = new DoctorDao();
            doctorDao.insertDoctor(doctor);
            RequestDispatcher requestDispatcher = req.getRequestDispatcher("index.html");
            printWriter.print("Doctor added succesfully");
            requestDispatcher.include(req, resp);
        } else {
            RequestDispatcher requestDispatcher = req.getRequestDispatcher("index.html");
            printWriter.print("Doctor already exists");
            requestDispatcher.include(req, resp);
        }

    }
}
