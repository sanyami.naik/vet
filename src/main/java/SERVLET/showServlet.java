package SERVLET;

import DAO.FactoryProvider;
import ENTITY.Doctor;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

@WebServlet("/showServlet")
public class showServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        PrintWriter printWriter=resp.getWriter();
        resp.setContentType("text/html");

        EntityManager entityManager = FactoryProvider.getEntityManagerFactory().createEntityManager();
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery criteriaQuery = criteriaBuilder.createQuery();
        Root<Doctor> root = criteriaQuery.from(Doctor.class);

        Query query = entityManager.createQuery(criteriaQuery.select(root));
        List<Doctor> doctorList = query.getResultList();

        for (Doctor d: doctorList) {
            printWriter.println(d+"<br>");
        }
    }
}
