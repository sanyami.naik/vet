package SERVLET;

import DAO.BookingDao;
import DAO.FactoryProvider;
import ENTITY.Appointment;
import ENTITY.Doctor;
import ENTITY.User;

import javax.persistence.EntityManager;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/BookingServlet")
public class BookingServlet  extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        PrintWriter printWriter = resp.getWriter();
        resp.setContentType("text/html");

        int uId=Integer.parseInt(req.getParameter("uId"));
        int dId=Integer.parseInt(req.getParameter("dId"));
        String pName=req.getParameter("petName");

        EntityManager entityManager = FactoryProvider.getEntityManagerFactory().createEntityManager();
        Doctor doctor=entityManager.find(Doctor.class,dId);
        User user=entityManager.find(User.class,uId);
        if(doctor!=null)
        {
            if(doctor.getStatus().equals("AVAILABLE"))
            {
                Appointment appointment=new Appointment(pName,doctor,user);
                BookingDao bookingDao=new BookingDao();
                bookingDao.insertBooking(appointment);
                RequestDispatcher requestDispatcher = req.getRequestDispatcher("index.html");
                printWriter.print("Appointment done!!");
                requestDispatcher.include(req, resp);
            }
            else {
                RequestDispatcher requestDispatcher = req.getRequestDispatcher("index.html");
                printWriter.print("Doctor is not available");
                requestDispatcher.include(req, resp);

            }
        }
        else {
            RequestDispatcher requestDispatcher = req.getRequestDispatcher("index.html");
            printWriter.print("Doctor doesnt exist");
            requestDispatcher.include(req, resp);

        }



    }
}
