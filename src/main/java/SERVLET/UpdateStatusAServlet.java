package SERVLET;


import DAO.DoctorDao;
import DAO.FactoryProvider;
import ENTITY.Doctor;

import javax.persistence.EntityManager;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/UpdateStatusAServlet")
public class UpdateStatusAServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        PrintWriter printWriter = resp.getWriter();
        resp.setContentType("text/html");

        int dId=Integer.parseInt(req.getParameter("dId"));
        String doctorPassword = req.getParameter("dPassword");


        EntityManager entityManager = FactoryProvider.getEntityManagerFactory().createEntityManager();
        Doctor doctor=entityManager.find(Doctor.class,dId);

        if(doctor!=null)
        {

            if(doctor.getDoctorPassword().equals(doctorPassword))
            {
                if(!doctor.getStatus().equals("AVAILABLE")) {
                    DoctorDao doctorDao = new DoctorDao();
                    doctorDao.updateAStatus(doctor);
                    RequestDispatcher requestDispatcher = req.getRequestDispatcher("index.html");
                    printWriter.print("Doctor status updated succesfully");
                    requestDispatcher.include(req, resp);
                }
                else {
                    RequestDispatcher requestDispatcher = req.getRequestDispatcher("index.html");
                    printWriter.print("Doctor status is already available");
                    requestDispatcher.include(req, resp);
                }
            }
        }

     else {
            RequestDispatcher requestDispatcher = req.getRequestDispatcher("index.html");
            printWriter.print("Doctor doesnt exist");
            requestDispatcher.include(req, resp);
        }
    }
}
