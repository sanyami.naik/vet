package ENTITY;

import javax.persistence.*;
import java.util.List;

@Entity
public class Doctor {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    int doctorId;
    String doctorName;
    String status;
    String doctorPassword;
    int doctorFees;

    Long doctorNumber;

    @OneToMany(mappedBy = "doctor")
    List<Appointment> appointmentList;

    public List<Appointment> getAppointmentList() {
        return appointmentList;
    }

    public void setAppointmentList(List<Appointment> appointmentList) {
        this.appointmentList = appointmentList;
    }

    public Long getDoctorNumber() {
        return doctorNumber;
    }

    public void setDoctorNumber(Long doctorNumber) {
        this.doctorNumber = doctorNumber;
    }

    public Doctor(String doctorName, String status, String doctorPassword, int doctorFees, Long doctorNumber) {
        this.doctorName = doctorName;
        this.status = status;
        this.doctorPassword = doctorPassword;
        this.doctorFees = doctorFees;
        this.doctorNumber = doctorNumber;
    }

    public Doctor() {
    }

    public int getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(int doctorId) {
        this.doctorId = doctorId;
    }

    public String getDoctorName() {
        return doctorName;
    }

    public void setDoctorName(String doctorName) {
        this.doctorName = doctorName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getDoctorFees() {
        return doctorFees;
    }

    public void setDoctorFees(int doctorFees) {
        this.doctorFees = doctorFees;
    }

    public String getDoctorPassword() {
        return doctorPassword;
    }

    public void setDoctorPassword(String doctorPassword) {
        this.doctorPassword = doctorPassword;
    }

    @Override
    public String toString() {
        return "Doctor{" +
                "doctorId=" + doctorId +
                ", doctorName='" + doctorName + '\'' +
                ", status='" + status + '\'' +
                ", doctorFees=" + doctorFees +
                '}';
    }
}
